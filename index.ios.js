'use strict';
// var React = require('react-native');



//  var {
//   AppRegistry,
//   StyleSheet,
//   Text,
//   View,
//   TouchableHighlight,
//   Component,
//   AlertIOS 
// } = React;

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  TouchableHighlight,
  Text,
  View,
  AlertIOS 
} from 'react-native';

var Firebase = require('firebase');

 var styles = StyleSheet.create({
    container: {
      flex: 1, 
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#ffffff'
    },
    buttonText: {
    fontSize: 18,
    color: 'white',
    alignSelf: 'center'
  },
  button: {
    height: 44,
    flexDirection: 'row',
    backgroundColor: '#48BBEC',
    alignSelf: 'stretch',
    justifyContent: 'center'
  }
 });
class members_react_native extends Component {
  // Your App Code
  constructor(props) {
    super(props);
    
    var config = {
    apiKey: "AIzaSyBqop4QrISCqwuGrASAyTdVqjFv3fMDqE0",
    authDomain: "members-1f2e7.firebaseapp.com",
    databaseURL: "https://members-1f2e7.firebaseio.com"
  };

  Firebase.initializeApp(config);

  var rootRef = Firebase.database().ref();


  this.itemsRef = rootRef.child('items');

  this.state = {
    newMember: '',
    memberList: new ListView.DataSource({rowHasChanged: (row1, row2) => row1 !== row2})
  };

  this.items = [];  

  //var myFirebaseRef = new Firebase('https://members-1f2e7.firebaseio.com/');
    



    rootRef.set({
           firstName: "Van", 
      lastName: "Vairavan",
      eMail: "vancreative@gmail.com"
       // location: {
      //   city: "Muenster",
      //   state: "Germany",
      //   zip: 48155
      // }
    });
  }


  render() {
    return (
      <View style={styles.appContainer}>
        <View style={styles.titleView}>
          <Text style={styles.titleText}>
            My Todos
          </Text>
        </View>
        <View style={styles.inputcontainer}>
          <TextInput style={styles.input} onChangeText={(text) => this.setState({newTodo: text})} value={this.state.newTodo}/>
          <TouchableHighlight
            style={styles.button}
            onPress={() => this.addTodo()}
            underlayColor='#dddddd'>
            <Text style={styles.btnText}>Add!</Text>
          </TouchableHighlight>
        </View>
        <ListView
          dataSource={this.state.todoSource}
          renderRow={this.renderRow.bind(this)} />
      </View>
    );
  }
 }

 componentDidMount() {
  // When a member is added
  this.itemsRef.on('child_added', (dataSnapshot) => {
    this.items.push({id: dataSnapshot.key(), text: dataSnapshot.val()});
    this.setState({
      memberList: this.state.memberList.cloneWithRows(this.items)
    });
  });
 
  // When a member is removed
  this.itemsRef.on('child_removed', (dataSnapshot) => {
      this.items = this.items.filter((x) => x.id !== dataSnapshot.key());
      this.setState({
        memberList: this.state.memberList.cloneWithRows(this.items)
      });
  });
}

addMember() {
  if (this.state.newMember !== '') {
    this.itemsRef.push({
      member: this.state.newMember
    });
    this.setState({
      newMember : ''
    });
  }
}

removeMember(rowData) {
  this.itemsRef.child(rowData.id).remove();
}

renderRow(rowData) {
  return (
    <TouchableHighlight
      underlayColor='#dddddd'
      onPress={() => this.removeMember(rowData)}>
      <View>
        <View style={styles.row}>
          <Text style={styles.todoText}>{rowData.text.todo}</Text>
        </View>
        <View style={styles.separator} />
      </View>
    </TouchableHighlight>
    );
}



  // showAlert() {
  //   AlertIOS.alert('Awesome Alert', 'This is my first React Native alert.', [{text: 'Thanks'}] )
  // }

 AppRegistry.registerComponent('members_react_native', () => members_react_native);
